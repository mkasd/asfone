package org.asfone.ui.config;

import org.asfone.middle.service.config.ServiceConfiguration;
import org.asfone.ui.security.config.SecurityConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = {"org.asfone.ui.controller.impl"})
@Import({ServiceConfiguration.class, SecurityConfiguration.class})
public class UiConfiguration {
}

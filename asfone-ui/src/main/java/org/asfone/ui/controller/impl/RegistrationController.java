package org.asfone.ui.controller.impl;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.asfone.core.dto.UserRole;
import org.asfone.core.exception.UserRegistrationException;
import org.asfone.core.model.User;
import org.asfone.middle.service.interfaces.IRegistrationService;
import org.asfone.middle.service.interfaces.IUserService;
import org.asfone.ui.controller.api.AbstractController;
import org.controlsfx.control.Notifications;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

public class RegistrationController extends AbstractController {

    @Autowired
    private IRegistrationService registrationService;

    @Autowired
    private IUserService userService;

    @FXML
    private Button registerButton;

    @FXML
    private TextField surnameField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    public void initialize(URL url, ResourceBundle rs) {
        initializeValidation();

        initializeFields();
    }

    public String windowTitle() {
        return "Реєстрація";
    }

    @Override
    protected String fxmlResource() {
        return "fxml/registrationView.fxml";
    }

    private void initializeFields() {
        registerButton.setOnAction(e -> {
            performRegistration();
        });
    }

    private void performRegistration() {
        String name = nameField.textProperty().get();
        String surname = surnameField.textProperty().get();
        String login = loginField.textProperty().get();
        String password = passwordField.textProperty().get();

        User user = new User();
        user.setLogin(login);
        user.setName(name);
        user.setPassword(password);
        user.setSurname(surname);
        user.setRole(UserRole.STUDENT);

        try {
            registrationService.registerUser(user);

            Notifications.create().title("Реєстрація завершена").text("Новий користувач доданий").showInformation();

            this.close();
            LoginController loginController = new LoginController();
            loginController.show();
        } catch (UserRegistrationException e) {
            Notifications.create().title("Помилка при реєстрації")
                    .text("Будь ласка, перевірте правилльність введених даних").showError();
        }
    }

    private void initializeValidation() {
        registerButton.setDisable(true);

        GraphicValidationDecoration validationDecoration = new GraphicValidationDecoration();
        validationDecoration.applyRequiredDecoration(surnameField);
        validationDecoration.applyRequiredDecoration(nameField);
        validationDecoration.applyRequiredDecoration(loginField);
        validationDecoration.applyRequiredDecoration(passwordField);

        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.setErrorDecorationEnabled(true);
        validationSupport.setValidationDecorator(validationDecoration);
        validationSupport.registerValidator(loginField, Validator.createEmptyValidator("Поле не повинно бути пустим"));
        validationSupport.registerValidator(nameField, Validator.createEmptyValidator("Поле не повинно бути пустим"));
        validationSupport
                .registerValidator(surnameField, Validator.createEmptyValidator("Поле не повинно бути пустим"));
        validationSupport.registerValidator(passwordField,
                Validator.createEmptyValidator("Поле не повинно бути пустим"));

        validationSupport.validationResultProperty().addListener((o, oldValue, newValue) -> {
            if (newValue.getErrors().isEmpty()) {
                registerButton.setDisable(false);
            } else {
                registerButton.setDisable(true);
            }
        });
    }
}

package org.asfone.ui.controller.impl;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.asfone.ui.controller.api.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

public class WelcomeController extends AbstractController {

    @FXML
    private Button oldUserButton;

    @FXML
    private Button newUserButton;

    public void initialize(URL arg0, ResourceBundle arg1) {
        oldUserButton.setOnAction(e -> {
            WelcomeController.this.close();

            LoginController loginController = new LoginController();
            loginController.show();
        });

        newUserButton.setOnAction(e -> {
            WelcomeController.this.close();

            RegistrationController registrationController = new RegistrationController();
            registrationController.show();
        });
    }

    public String windowTitle() {
        return "Головна";
    }

    @Override
    protected String fxmlResource() {
        return "fxml/welcomeView.fxml";
    }
}

package org.asfone.ui.controller.impl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import org.asfone.core.model.Publication;
import org.asfone.middle.service.interfaces.IPublicationService;
import org.asfone.ui.component.custom.PublicationItem;
import org.asfone.ui.controller.api.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PublicationListController extends AbstractController {

    @Autowired
    private IPublicationService publicationService;

    @FXML
    private Button newDocButton;

    @FXML
    private ListView<Publication> publicationItemsContainer;

    public void initialize(URL arg0, ResourceBundle arg1) {
        initFields();
    }

    public String windowTitle() {
        return "Список документів";
    }

    @Override
    protected String fxmlResource() {
        return "fxml/publicationListView.fxml";
    }


    private void initFields() {
        initButtons();

        initListView();
    }

    private void initListView() {
        List<Publication> publications = publicationService.findAll();
        ObservableList<Publication> data = FXCollections.observableArrayList(publications);

        publicationItemsContainer.setItems(data);
        publicationItemsContainer.setCellFactory(param -> new PublicationListCell());
    }

    private void initButtons() {
        newDocButton.setOnAction(e -> {
            AddNewDocumentController addNewDocumentController = new AddNewDocumentController();
            addNewDocumentController.show();
        });
    }

    private static class PublicationListCell extends ListCell<Publication> {

        @Override
        protected void updateItem(Publication publication, boolean empty) {
            PublicationItem publicationItem = new PublicationItem(publication);

            setGraphic(publicationItem);
        }

    }
}

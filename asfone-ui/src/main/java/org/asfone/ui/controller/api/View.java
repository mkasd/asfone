package org.asfone.ui.controller.api;

import javafx.stage.Stage;

public interface View {

    public void show();

    public void close();

    public Stage getMainStage();

    public String windowTitle();
}

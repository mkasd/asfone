package org.asfone.ui.controller.api;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.ui.component.loader.SpringFxmlLoader;

import java.io.IOException;

public abstract class AbstractController extends AnchorPane implements Initializable, View {

    protected static final Logger logger = LogManager.getLogger(AbstractController.class.getSimpleName());

    private static final SpringFxmlLoader loader = new SpringFxmlLoader();

    private Stage mainStage;

    public AbstractController() {
        super();
        Parent parent = loader.load(fxmlResource(), mainStage, getController());
        Scene scene = new Scene(parent);
        mainStage = new Stage();
        mainStage.setTitle(windowTitle());
        mainStage.initModality(Modality.APPLICATION_MODAL);
        mainStage.initOwner(scene.getWindow());
        mainStage.setScene(scene);

        logger.info("Created new instance of " + this.getClass().getSimpleName());
    }

    public void show() {
        mainStage.show();
    }

    public void close() {
        mainStage.hide();
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public Object getController() {
        return this;
    }

    protected abstract String fxmlResource();
}

package org.asfone.ui.controller.impl;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.asfone.core.dto.Category;
import org.asfone.core.dto.DocumentType;
import org.asfone.core.model.Publication;
import org.asfone.core.model.User;
import org.asfone.middle.service.interfaces.IPublicationService;
import org.asfone.middle.service.interfaces.IUserService;
import org.asfone.ui.component.converter.CategoryStringConverter;
import org.asfone.ui.controller.api.AbstractController;
import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class AddNewDocumentController extends AbstractController {

    @Autowired
    private IPublicationService publicationService;

    @Autowired
    private IUserService userService;

    @FXML
    private TextArea titleTextArea;

    @FXML
    private TextField yearField;

    @FXML
    private TextField authorField;

    @FXML
    private TextArea descTextArea;

    @FXML
    private ChoiceBox<Category> categorySelector;

    @FXML
    private Button chooseFileButton;

    @FXML
    private Button addDocumentButton;

    private File selectedFile;

    public void initialize(URL arg0, ResourceBundle arg1) {
        categorySelector.setItems(FXCollections.observableArrayList(Category.ARTICLE, Category.BOOK,
                Category.SCIENCE_PUBLICATION_LIST));
        categorySelector.setValue(Category.ARTICLE);
        categorySelector.setConverter(new CategoryStringConverter());

        chooseFileButton.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Оберіть документ");
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("PDF", getFileMask(DocumentType.PDF)),
                        new FileChooser.ExtensionFilter("DOC", getFileMask(DocumentType.DOC)),
                        new FileChooser.ExtensionFilter("DOCX", getFileMask(DocumentType.DOCX)));

                selectedFile = fileChooser.showOpenDialog(AddNewDocumentController.this.getMainStage());
            }

            private String getFileMask(DocumentType documentType) {
                return "*" + documentType.getExtension();
            }
        });

        addDocumentButton.setOnAction(new EventHandler<ActionEvent>() {

            public void handle(ActionEvent arg0) {
                String login = SecurityContextHolder.getContext().getAuthentication().getName();
                User user = userService.findByLogin(login);

                Date publishYear;
                try {
                    publishYear = new SimpleDateFormat("yyyy").parse(yearField.textProperty().get());
                } catch (ParseException e) {
                    publishYear = new Date();

                    Notifications.create().title("Помилка").text("Не вдалось розпізнати рік видання").showWarning();
                }

                Publication publication = new Publication();
                publication.setCategory(categorySelector.selectionModelProperty().get().getSelectedItem());
                publication.setTitle(titleTextArea.textProperty().get());
                publication.setAuthor(authorField.textProperty().get());
                publication.setPublishYear(publishYear);
                publication.setPublishDate(new Date());
                publication.setShortDescription(descTextArea.textProperty().get());

                publicationService.savePublication(publication, selectedFile);

                Notifications.create().title("Документ додано").text("Тепер документ доступний для використання")
                        .showInformation();

                AddNewDocumentController.this.close();
            }
        });
    }

    public String windowTitle() {
        return "Додати новий документ";
    }

    @Override
    protected String fxmlResource() {
        return "fxml/addNewDocumentView.fxml";
    }

}

package org.asfone.ui.controller.impl;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.asfone.core.exception.AuthenticationException;
import org.asfone.ui.controller.api.AbstractController;
import org.asfone.ui.security.service.AuthorizationService;
import org.controlsfx.control.Notifications;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends AbstractController {

    @Autowired
    private AuthorizationService authorizationService;

    @FXML
    private Button loginButton;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    public void initialize(URL arg0, ResourceBundle arg1) {
        initializeValidation();

        initializeFields();
    }

    private void initializeFields() {
        loginButton.setOnAction(event -> {
            String login = loginField.textProperty().get();
            String password = passwordField.textProperty().get();

            try {
                authorizationService.login(login, password);

                LoginController.this.close();

                PublicationListController publicationListController = new PublicationListController();
                publicationListController.show();
            } catch (AuthenticationException e) {
                Notifications.create().title("Помилка при вході в систему")
                        .text("Перевірте правильність введених данних");
            }
        });

    }

    private void initializeValidation() {
        loginButton.setDisable(true);

        GraphicValidationDecoration validationDecoration = new GraphicValidationDecoration();
        validationDecoration.applyRequiredDecoration(loginField);
        validationDecoration.applyRequiredDecoration(passwordField);

        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.setErrorDecorationEnabled(true);
        validationSupport.setValidationDecorator(validationDecoration);
        validationSupport.registerValidator(loginField, Validator.createEmptyValidator("Поле не повинно бути пустим"));
        validationSupport.registerValidator(passwordField,
                Validator.createEmptyValidator("Поле не повинно бути пустим"));

        validationSupport.validationResultProperty().addListener((o, oldValue, newValue) -> {
            if (newValue.getErrors().isEmpty()) {
                loginButton.setDisable(false);
            } else {
                loginButton.setDisable(true);
            }
        });
    }

    public String windowTitle() {
        return "Сторінка входу";
    }

    @Override
    protected String fxmlResource() {
        return "fxml/loginView.fxml";
    }
}

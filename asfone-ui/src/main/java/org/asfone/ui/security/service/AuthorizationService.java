package org.asfone.ui.security.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.core.exception.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationService {

    private static final Logger log = LogManager.getLogger(AuthorizationService.class.getSimpleName());

    private AuthenticationManager authenticationManager;

    @Autowired
    public AuthorizationService(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Performs user authentication
     *
     * @param login    - user login
     * @param password - user password
     */
    public void login(String login, String password) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);

            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            log.info("Authorization performed successfully");
        } catch (InternalAuthenticationServiceException e) {
            log.warn("Authentication failed for user <" + login + ">");
            throw new AuthenticationException("Error while performing authentication", e);
        }
    }
}

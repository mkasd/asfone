package org.asfone.ui.component.loader;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import org.asfone.ui.config.UiConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public class SpringFxmlLoader {

    private final static ApplicationContext context = new AnnotationConfigApplicationContext(UiConfiguration.class);

    public Parent load(String url, Stage mainStage, Object controller) {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(url));
        loader.setRoot(mainStage);
        loader.setController(controller);
        loader.setControllerFactory(context::getBean);
        try {
            return (Parent) loader.load();
        } catch (IOException e) {
            throw new RuntimeException("Could not load 'FXML' resource. Reason: ", e);
        }
    }
}

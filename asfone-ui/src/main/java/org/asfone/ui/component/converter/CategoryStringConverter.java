package org.asfone.ui.component.converter;

import javafx.util.StringConverter;
import org.asfone.core.dto.Category;

public class CategoryStringConverter extends StringConverter<Category> {

    @Override
    public Category fromString(String categoryValue) {
        return Category.valueOf(categoryValue);
    }

    @Override
    public String toString(Category category) {
        return category.getName();
    }

}

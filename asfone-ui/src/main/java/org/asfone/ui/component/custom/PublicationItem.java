package org.asfone.ui.component.custom;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import org.asfone.core.model.Publication;

import java.io.IOException;

public class PublicationItem extends AnchorPane {

    private static final String resName = "fxml/publicationItem.fxml";

    private static final String imageFolder = "img/";

    @FXML
    private ImageView formatImage;

    @FXML
    private Label titleLabel;

    @FXML
    private Hyperlink moreLink;

    public PublicationItem(Publication publication) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(resName));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        fillComponent(publication);
    }

    private void fillComponent(Publication publication) {
        String extension = publication.getDocumentType().getExtension().substring(1);

        setImage(extension);
        setTitle(publication.getTitle());
    }

    private void setImage(String extension) {
        String resourceName = getResourceName(extension);
        Image image = new Image(getClass().getResourceAsStream(resourceName));

        formatImage.setImage(image);
    }

    private void setTitle(String title) {
        titleLabel.textProperty().set(title);
    }

    private String getResourceName(String extension) {
        return imageFolder + extension + ".png";
    }
}

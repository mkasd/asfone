package org.asfone.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import org.asfone.ui.config.UiConfiguration;
import org.asfone.ui.controller.impl.WelcomeController;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Runner extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage arg0) throws Exception {
        WelcomeController controller = new WelcomeController();
        controller.show();
    }
}

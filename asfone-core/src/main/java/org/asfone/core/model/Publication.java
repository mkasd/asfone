package org.asfone.core.model;

import org.asfone.core.dto.Category;
import org.asfone.core.dto.DocumentType;

import javax.persistence.*;
import java.util.Date;

/**
 * @author oleksandr.dibrivnyi
 */
@Table(name = "PUBLICATION")
@Entity
public class Publication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "PAGES_COUNT", nullable = false)
    private int pagesCount;

    @Enumerated(EnumType.STRING)
    @Column(name = "CATEGORY", nullable = false)
    private Category category;

    @Column(name = "PUBLISH_YEAR", nullable = false)
    private Date publishYear;

    @Column(name = "AUTHOR", nullable = false)
    private String author;

    @Column(name = "PRODUCER", nullable = false)
    private String producer;

    @Column(name = "SHORT_DESC", nullable = false)
    private String shortDescription;

    @Enumerated(EnumType.STRING)
    @Column(name = "DOC_TYPE", nullable = false)
    private DocumentType documentType;

    @Column(name = "STORE_PATH", nullable = false)
    private String storePath;

    @Column(name = "PUBLISH_DATE", nullable = false)
    private Date publishDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Date publishYear) {
        this.publishYear = publishYear;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Publication that = (Publication) o;

        if (id != that.id) return false;
        if (pagesCount != that.pagesCount) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (category != that.category) return false;
        if (documentType != that.documentType) return false;
        if (producer != null ? !producer.equals(that.producer) : that.producer != null) return false;
        if (publishDate != null ? !publishDate.equals(that.publishDate) : that.publishDate != null) return false;
        if (publishYear != null ? !publishYear.equals(that.publishYear) : that.publishYear != null) return false;
        if (shortDescription != null ? !shortDescription.equals(that.shortDescription) : that.shortDescription != null)
            return false;
        if (storePath != null ? !storePath.equals(that.storePath) : that.storePath != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + pagesCount;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (publishYear != null ? publishYear.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (producer != null ? producer.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        result = 31 * result + (documentType != null ? documentType.hashCode() : 0);
        result = 31 * result + (storePath != null ? storePath.hashCode() : 0);
        result = 31 * result + (publishDate != null ? publishDate.hashCode() : 0);
        return result;
    }
}

package org.asfone.core.dto;

public abstract class Naming {

    public abstract class UserModel {
        public static final String ID = "ID";

        public static final String NAME = "NAME";

        public static final String SURNAME = "SURNAME";

        public static final String ROLE = "ROLE";
    }

    public abstract class GroupModel {
        public static final String ID = "GROUP_ID";

        public static final String NAME = "GROUP_NAME";
    }

    public abstract class PublicationModel {
        public static final String ID = "ID";

        public static final String TITLE = "TITLE";

        public static final String CATEGORY = "CATEGORY";

        public static final String AUTHOR = "AUTHOR";

        public static final String PRODUCER = "PRODUCER";

        public static final String PUBLISH_YEAR = "PUBLISH_YEAR";

        public static final String SHORT_DESCRIPTION = "SHORT_DESC";

        public static final String DOCUMENT_TYPE = "DOC_TYPE";

        public static final String STORE_PATH = "STORE_PATH";

        public static final String PUBLISHED_USER_ID = "USER_ID";

        public static final String PUBLISHED_USER_NAME = "USER_NAME";

        public static final String PUBLISHED_USER_SURNAME = "USER_SURNAME";
    }
}

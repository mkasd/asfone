package org.asfone.core.dto;

/**
 * Describes the user role: 1. If user the teacher - it have all permissions 2.
 * If user the student - it can perform only read or calculate data
 *
 * @author oleksandr.dibrivnyi
 */
public enum UserRole {

    ADMIN, STUDENT
}

package org.asfone.core.dto;

/**
 * Holds database and files storage path
 *
 * @author oleksandr.dibrivnyi
 */
public class ApplicationPaths {

    private String filesPath;

    public ApplicationPaths(String filesPath) {
        this.filesPath = filesPath;
    }

    public String getFilesPath() {
        return filesPath;
    }
}

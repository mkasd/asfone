package org.asfone.core.dto;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public class FieldRequest {

    private String fieldName;
    private String fieldValue;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}

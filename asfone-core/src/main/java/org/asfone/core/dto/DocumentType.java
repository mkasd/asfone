package org.asfone.core.dto;

/**
 * Describes possible variants of document types and their extensions
 *
 * @author oleksandr.dibrivnyi
 */
public enum DocumentType {
    PDF(".pdf"), DOC(".doc"), DOCX(".docx");

    private String extension;

    private DocumentType(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}

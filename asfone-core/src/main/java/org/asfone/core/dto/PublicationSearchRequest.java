package org.asfone.core.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public class PublicationSearchRequest {

    private List<FieldRequest> requestData = new ArrayList<>();

    public void addRequestData(FieldRequest fieldRequest) {
        requestData.add(fieldRequest);
    }

    public void addRequestDatas(FieldRequest... fieldRequests) {
        if (fieldRequests != null) {
            Collections.addAll(requestData, fieldRequests);
        }
    }

    public List<FieldRequest> getRequestData() {
        return requestData;
    }
}

package org.asfone.core.dto;

/**
 * Describes possible types of documents
 *
 * @author oleksandr.dibrivnyi
 */
public enum Category {

    BOOK("Книга"),
    ARTICLE("Стаття"),
    SCIENCE_PUBLICATION_LIST("Збірник наукових праць"),
    THESIS("Автореферат дисертації"),
    COPYRIGHT_CERTIFICATE("Авторське свідоцтво"),
    PATENT("Патент"),
    STANDARD("Стандарт"),
    INTERNET_RESOURCE("Електронний ресурс");

    private String name;

    Category(String name) {
        this.name = name;
    }

    /**
     * @return readable name of selected document type
     */
    public String getName() {
        return name;
    }
}

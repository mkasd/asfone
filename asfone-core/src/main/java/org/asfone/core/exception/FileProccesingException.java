package org.asfone.core.exception;

public class FileProccesingException extends RuntimeException {

    public FileProccesingException() {
    }

    public FileProccesingException(String message) {
        super(message);
    }

    public FileProccesingException(Throwable cause) {
        super(cause);
    }

    public FileProccesingException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileProccesingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

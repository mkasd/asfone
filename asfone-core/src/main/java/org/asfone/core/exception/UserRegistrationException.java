package org.asfone.core.exception;

public class UserRegistrationException extends RuntimeException {

    public UserRegistrationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public UserRegistrationException(String arg0) {
        super(arg0);
    }

}

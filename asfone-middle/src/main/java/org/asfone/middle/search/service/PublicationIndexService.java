package org.asfone.middle.search.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.asfone.core.dto.Naming;
import org.asfone.core.model.Publication;
import org.asfone.middle.search.converter.PublicationToDocumentConverter;
import org.asfone.middle.search.exception.IndexException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PublicationIndexService {

    private static final Logger log = LogManager.getLogger(PublicationIndexService.class.getSimpleName());

    private PublicationToDocumentConverter converter;

    private IndexWriter indexWriter;

    @Autowired
    public PublicationIndexService(IndexWriter indexWriter, PublicationToDocumentConverter converter) {
        this.indexWriter = indexWriter;
        this.converter = converter;
    }

    public void indexPublication(Publication publication) throws IndexException {
        Document luceneDocument = converter.convert(publication);
        try {
            indexWriter.addDocument(luceneDocument);

            log.info("Publication {0} was added to search indexes", publication.getId());
        } catch (IOException e) {
            throw new IndexException("Could not add new publication: ", e);
        }
    }

    public void deletePublication(long id) throws IndexException {
        Term deleteTerm = new Term(Naming.PublicationModel.ID, String.valueOf(id));
        try {
            indexWriter.deleteDocuments(deleteTerm);

            log.info("Publication {0} was deleted from search indexes", id);
        } catch (IOException e) {
            throw new IndexException("Could not delete publication: ", e);
        }
    }
}

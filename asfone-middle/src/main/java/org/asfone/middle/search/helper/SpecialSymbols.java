package org.asfone.middle.search.helper;

public interface SpecialSymbols {
    String KW_OR = ";";
    String KW_NOT = "-";
}

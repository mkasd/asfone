package org.asfone.middle.search.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.util.Bits;
import org.asfone.core.dto.FieldRequest;
import org.asfone.core.dto.Naming;
import org.asfone.core.dto.PublicationSearchRequest;
import org.asfone.middle.search.exception.SearchException;
import org.asfone.middle.search.helper.LuceneQueryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
@Service
public class PublicationSearchService {

    private static final Logger log = LogManager.getLogger(PublicationSearchService.class.getSimpleName());

    private IndexReader indexReader;

    private LuceneQueryHelper luceneQueryHelper;

    @Autowired
    public PublicationSearchService(IndexReader indexReader, LuceneQueryHelper luceneQueryHelper) {
        this.indexReader = indexReader;
        this.luceneQueryHelper = luceneQueryHelper;
    }

    /**
     * Finds list of ids for all managed objects
     *
     * @return list of ids for all managed objects
     * @throws SearchException - throws if there are any undefined behaviour
     */
    public List<Long> listAll() throws SearchException {
        Bits liveDocs = MultiFields.getLiveDocs(indexReader);
        if (liveDocs == null) {
            return Collections.<Long>emptyList();
        } else {
            int documentsCount = indexReader.maxDoc();
            List<Long> documentsIds = new ArrayList<>();

            for (int i = 0; i < documentsCount; i++) {
                if (liveDocs.get(i)) {
                    try {
                        Document document = indexReader.document(i);
                        String documentId = document.get(Naming.PublicationModel.ID);

                        documentsIds.add(Long.parseLong(documentId));
                    } catch (IOException e) {
                        throw new SearchException("Could not get full list of stored documents. Reason: ", e);
                    }
                }
            }
            return documentsIds;
        }
    }

    /**
     * Finds document ids that matches specified criteria
     *
     * @param searchRequest - search criteria
     * @return - list of documents id
     * @throws SearchException - throws if there are any undefined behaviour
     */
    public List<Long> findForRequest(PublicationSearchRequest searchRequest) throws SearchException {
        List<FieldRequest> requestData = searchRequest.getRequestData();

        if (requestData.isEmpty()) {
            return Collections.<Long>emptyList();
        } else {
            BooleanQuery topQuery = new BooleanQuery();

            for (FieldRequest fieldRequest : requestData) {
                try {
                    Query fieldQuery = luceneQueryHelper.query(fieldRequest.getFieldName(), fieldRequest.getFieldValue());
                    topQuery.add(fieldQuery, BooleanClause.Occur.SHOULD);
                } catch (ParseException e) {
                    log.warn("Cannot add field value to search request. Reason: ", e);
                }
            }
            TopScoreDocCollector docCollector = TopScoreDocCollector.create(Integer.MAX_VALUE, true);
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);

            try {
                indexSearcher.search(topQuery, docCollector);

                ScoreDoc[] scoreDocs = docCollector.topDocs().scoreDocs;
                return transformToDocumentIdsCollection(scoreDocs, indexSearcher);
            } catch (IOException e) {
                throw new SearchException("Cannot find documents for search request. Reason: ", e);
            }
        }
    }

    /**
     * Finds document id for the first document that matches specified criteria
     *
     * @param searchRequest - search criteria
     * @return - document id
     * @throws SearchException - throws if there are any undefined behaviour
     */
    public Long findFirstForRequest(PublicationSearchRequest searchRequest) throws SearchException {
        List<Long> documentIds = findForRequest(searchRequest);
        if (documentIds.isEmpty()) {
            return null;
        } else {
            return documentIds.get(0);
        }
    }

    private List<Long> transformToDocumentIdsCollection(ScoreDoc[] scoreDocs, IndexSearcher indexSearcher) throws SearchException {
        List<Long> documentsIdList = new ArrayList<>();
        for (ScoreDoc scoreDoc : scoreDocs) {
            int documentIndexId = scoreDoc.doc;
            try {
                Document document = indexSearcher.doc(documentIndexId);
                String docId = document.get(Naming.PublicationModel.ID);

                documentsIdList.add(Long.parseLong(docId));
            } catch (IOException e) {
                throw new SearchException("Cannot load document information. Reason: ", e);
            }
        }
        return documentsIdList;
    }
}

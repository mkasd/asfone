package org.asfone.middle.search.exception;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public class SearchException extends Exception {

    public SearchException(String s) {
        super(s);
    }

    public SearchException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

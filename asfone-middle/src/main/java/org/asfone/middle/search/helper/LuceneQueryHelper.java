package org.asfone.middle.search.helper;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.analyzing.AnalyzingQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LuceneQueryHelper {

    private static final String[] RESERVED_WORDS = {"AND", "NOT", "OR"};
    private Version version;
    private Analyzer luceneAnalyzer;

    private QueryParser.Operator defaultOperator;

    @Autowired
    public LuceneQueryHelper(Version version, Analyzer luceneAnalyzer) {
        this.luceneAnalyzer = luceneAnalyzer;
        this.version = version;
        this.defaultOperator = QueryParser.Operator.AND;
    }

    public boolean validateAndAdd(BooleanQuery dst, Query src, BooleanClause.Occur occur) {
        if ((src != null) && ((!src.toString().isEmpty()))) {
            dst.add(src, occur);
            return true;
        }
        return false;
    }

    public Query query(String field, String value) throws ParseException {
        if (isSingleTermQuery(value)) {
            return query(field, value, isQuoted(value));
        } else if (isWildcardQuery(value)) {
            return processWildcardQuery(field, value);
        } else {
            return booleanQuery(field, value);
        }
    }

    public Query query(String field, String value, boolean quote) throws ParseException {
        if (isWildcardQuery(value)) {
            return processWildcardQuery(field, value);
        }
        String escaped = escapeQueryValue(value);
        QueryParser queryParser = new AnalyzingQueryParser(version, field, luceneAnalyzer);
        queryParser.setDefaultOperator(defaultOperator);
        queryParser.setAnalyzeRangeTerms(true);
        if (quote) {
            escaped = String.format("\"%s\"", escaped);
        }
        return queryParser.parse(escaped);
    }

    private String escapeQueryValue(String value) {
        value = QueryParser.escape(value);
        for (String reservedWord : RESERVED_WORDS) {
            value = value.replace(reservedWord, reservedWord.toLowerCase());
        }
        return value;
    }

    protected Query processWildcardQuery(String field, String value) throws ParseException {
        Query query;
        if (value.contains(" ")) {
            query = booleanQuery(field, value);
        } else {
            query = wildcardQuery(field, value);
        }
        return query;
    }

    private BooleanQuery booleanQuery(String field, String value) throws ParseException {
        BooleanQuery complexQuery = new BooleanQuery();
        if (isQuoted(value)) {
            BooleanClause.Occur occur = occur(value);
            if (BooleanClause.Occur.MUST_NOT.equals(occur)) {
                validateAndAdd(complexQuery, query(field, value, true), occur);
            }
        } else {
            queryByWords(field, value, complexQuery);
        }
        return complexQuery;
    }

    private void queryByWords(String field, String value, BooleanQuery complexQuery) throws ParseException {
        String[] quotes = getQuotes(value);
        if ((quotes != null) && (quotes.length > 0)) {
            for (String w : quotes) {
                validateAndAdd(complexQuery, query(field, w, true), quoteOccur(w, value));
                value = StringUtils.remove(value, "\"" + w + "\"").replaceAll("\\s+", " ");
            }
        }
        String[] words = value.split(" ");
        for (String w : words) {
            if (w.length() > 1) {
                Query q = query(field, w, false);
                validateAndAdd(complexQuery, q, occur(w));
            }
        }
    }

    public BooleanClause.Occur occur(String value) {
        return isException(value) ? BooleanClause.Occur.MUST_NOT : BooleanClause.Occur.MUST;
    }

    private BooleanClause.Occur quoteOccur(String value, String fullSearchString) {
        return isQuotationException(value, fullSearchString) ? BooleanClause.Occur.MUST_NOT : BooleanClause.Occur.MUST;
    }

    protected Query wildcardQuery(String field, String value) throws ParseException {
        String s = cleanWildcard(value.toLowerCase());
        Term term = s.isEmpty() ? new Term("") : new Term(field, s);
        return new WildcardQuery(term);
    }

    protected String cleanWildcard(String value) {
        for (char c : value.toCharArray()) {
            if ((c != '*') && (c != '?') && (!Character.isLetter(c))) {
                value = StringUtils.remove(value, c);
            }
        }
        return value;
    }

    protected boolean isWildcardQuery(String value) {
        if ((value == null) || (value.isEmpty())) {
            return false;
        }
        value = value.trim();
        if (isQuoted(value)) {
            return false;
        }

        boolean res = false;
        for (String v : value.split(" ")) {
            String tv = v.trim();
            if ((tv.indexOf('*') > 0) || (tv.indexOf('?') > 0)) {
                res = true;
                break;
            }
        }
        return res;
    }

    public boolean isException(String value) {
        return (value != null) && value.trim().startsWith(SpecialSymbols.KW_NOT);
    }

    public boolean isQuoted(String value) {
        String v = value.trim();
        return v.startsWith("\"") && (v.endsWith("\""));
    }

    public boolean isQuotationException(String quotation, String srcQuery) {
        srcQuery = srcQuery.replaceAll("\\s+", " ").replaceAll(SpecialSymbols.KW_NOT + " ", SpecialSymbols.KW_NOT);
        return srcQuery.contains(String.format("%s\"%s\"", SpecialSymbols.KW_NOT, quotation));
    }

    public boolean isSingleTermQuery(String value) {
        String str = value.trim();
        return !isException(str) && (isQuoted(str) || !str.contains(" "));
    }

    protected String[] getQuotes(String value) {
        return StringUtils.substringsBetween(value, "\"", "\"");
    }
}

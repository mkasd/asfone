package org.asfone.middle.search.exception;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public class IndexException extends Exception {

    public IndexException(String s) {
        super(s);
    }

    public IndexException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

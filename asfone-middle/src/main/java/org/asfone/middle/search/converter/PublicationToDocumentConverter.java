package org.asfone.middle.search.converter;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.asfone.core.dto.Naming;
import org.asfone.core.model.Publication;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class PublicationToDocumentConverter implements Converter<Publication, Document> {

    @Override
    public Document convert(Publication pub) {
        Document luceneDocument = new Document();
        luceneDocument.add(longField(Naming.PublicationModel.ID, pub.getId(), Store.YES));
        luceneDocument.add(textField(Naming.PublicationModel.TITLE, pub.getTitle(), Store.YES));
        luceneDocument.add(textField(Naming.PublicationModel.SHORT_DESCRIPTION, pub.getShortDescription(), Store.YES));
        luceneDocument.add(textField(Naming.PublicationModel.AUTHOR, pub.getAuthor(), Store.YES));
        luceneDocument.add(textField(Naming.PublicationModel.PRODUCER, pub.getProducer(), Store.YES));
        luceneDocument.add(dateField(Naming.PublicationModel.PUBLISH_YEAR, pub.getPublishYear(), Store.YES));
        luceneDocument.add(textField(Naming.PublicationModel.CATEGORY, pub.getCategory().getName(), Store.YES));
        return luceneDocument;
    }

    private Field textField(String fieldName, String value, Store store) {
        return new Field(fieldName, value, store, Index.ANALYZED);
    }

    private Field longField(String fieldName, long id, Store store) {
        return new Field(fieldName, String.valueOf(id), store, Index.ANALYZED);
    }

    private Field dateField(String fieldName, Date publishDate, Store store) {
        return new Field(fieldName, publishDate.toString(), store, Index.ANALYZED);
    }
}

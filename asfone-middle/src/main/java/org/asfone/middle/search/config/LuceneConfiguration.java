package org.asfone.middle.search.config;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.asfone.core.dto.ApplicationPaths;
import org.asfone.middle.helper.config.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

import java.io.File;
import java.io.IOException;

@Configuration
@ComponentScan(basePackages = {"org.asfone.middle.search.helper", "org.asfone.middle.search.service", "org.asfone.middle.search.converter"})
@Import({PropertiesConfiguration.class})
public class LuceneConfiguration {

    @Autowired
    private ApplicationPaths applicationPaths;

    @Bean
    public IndexWriter indexWriter() {
        IndexWriter indexWriter;
        try {
            indexWriter = new IndexWriter(fsStoreDirectory(), indexWriterConfig());
            indexWriter.commit();
        } catch (IOException e) {
            throw new RuntimeException("Unable to create lucene index writer: ", e);
        }
        return indexWriter;
    }

    @Bean
    @DependsOn("indexWriter")
    public IndexReader indexReader() {
        try {
            return DirectoryReader.open(fsStoreDirectory());
        } catch (IOException e) {
            throw new RuntimeException("Unable to create lucene index reader: ", e);
        }
    }

    @Bean
    public IndexWriterConfig indexWriterConfig() {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(version(), luceneAnalyzer());
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        return indexWriterConfig;
    }

    @Bean
    public Directory fsStoreDirectory() {
        try {
            File rootStoreDirectory = new File(applicationPaths.getFilesPath());
            if (!rootStoreDirectory.exists()) {
                rootStoreDirectory.mkdir();
            }
            return FSDirectory.open(rootStoreDirectory);
        } catch (IOException e) {
            throw new RuntimeException("Unable to create lucene index directory: ", e);
        }
    }

    @Bean
    public Analyzer luceneAnalyzer() {
        return new StandardAnalyzer(version());
    }

    @Bean
    public Version version() {
        return Version.LUCENE_40;
    }
}

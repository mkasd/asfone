package org.asfone.middle.service.impl;

import org.asfone.core.dto.PublicationSearchRequest;
import org.asfone.core.model.Publication;
import org.asfone.middle.search.exception.SearchException;
import org.asfone.middle.search.service.PublicationSearchService;
import org.asfone.middle.service.interfaces.IPublicationService;
import org.asfone.middle.service.interfaces.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
@Service
public class SearchService implements ISearchService {

    private PublicationSearchService publicationSearchService;
    private IPublicationService publicationService;

    @Autowired
    public SearchService(PublicationSearchService publicationSearchService, IPublicationService publicationService) {
        this.publicationSearchService = publicationSearchService;
        this.publicationService = publicationService;
    }

    @Override
    public List<Publication> listAll() throws SearchException {
        List<Long> docsIds = publicationSearchService.listAll();
        return publicationService.findAll(docsIds);
    }

    @Override
    public Publication findOne(PublicationSearchRequest searchRequest) throws SearchException {
        Long singleDocId = publicationSearchService.findFirstForRequest(searchRequest);
        return publicationService.findPublication(singleDocId);
    }

    @Override
    public List<Publication> findMultiple(PublicationSearchRequest searchRequest) throws SearchException {
        List<Long> documentIds = publicationSearchService.findForRequest(searchRequest);
        return publicationService.findAll(documentIds);
    }
}

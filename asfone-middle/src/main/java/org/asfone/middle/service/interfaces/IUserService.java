package org.asfone.middle.service.interfaces;

import org.asfone.core.model.User;

import java.util.List;

public interface IUserService {

    void deleteUser(long id);

    User findById(long id);

    User findByLogin(String login);

    List<String> findAllLogins();
}

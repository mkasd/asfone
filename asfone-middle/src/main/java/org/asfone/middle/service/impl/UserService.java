package org.asfone.middle.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.core.exception.UserNotFoundException;
import org.asfone.core.model.User;
import org.asfone.middle.dao.impl.UserRepository;
import org.asfone.middle.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    private static final Logger log = LogManager.getLogger(UserService.class.getSimpleName());

    private UserRepository userRepository;

    /**
     * Inject point of @link{UserDao}
     *
     * @param userDao - object to inject
     */
    @Autowired
    public void setUserDao(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Removes @link{User} record with specified id from database
     */
    @Override
    public void deleteUser(long id) {
        userRepository.delete(id);

        log.info("User with id {0} was successfully deleted from database", id);
    }

    /**
     * Finds @link{User} record with specified id
     */
    @Override
    public User findById(long id) {
        User foundUser = userRepository.findOne(id);
        if (foundUser != null) {
            return foundUser;
        } else {
            throw new UserNotFoundException("User with id <" + id + "> not found");
        }
    }

    /**
     * Finds @link{User} record with specified login
     */
    @Override
    public User findByLogin(String login) {
        User foundUser = userRepository.findByLogin(login);
        if (foundUser != null) {
            return foundUser;
        } else {
            throw new UserNotFoundException("User with login <" + login + "> not found");
        }
    }

    @Override
    public List<String> findAllLogins() {
        return userRepository.findAllLogins();
    }
}

package org.asfone.middle.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.core.exception.UserRegistrationException;
import org.asfone.core.model.User;
import org.asfone.middle.dao.impl.UserRepository;
import org.asfone.middle.service.interfaces.IRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService implements IRegistrationService {

    private static final Logger log = LogManager.getLogger(RegistrationService.class.getSimpleName());

    private UserRepository userRepository;

    @Autowired
    public RegistrationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registerUser(User user) {
        User savedUser = userRepository.save(user);
        if (savedUser != null) {
            log.info(String.format("New user <%s> was registered", user.getLogin()));
        } else {
            throw new UserRegistrationException("Error while register new user");
        }
    }

}

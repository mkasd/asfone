package org.asfone.middle.service.interfaces;

import org.asfone.core.model.Publication;

import java.io.File;
import java.util.List;

public interface IPublicationService {

    void savePublication(Publication doc, File documentFile);

    void deletePublication(long id);

    Publication findPublication(long id);

    List<Publication> findAll();

    List<Publication> findAll(Iterable<Long> ids);
}

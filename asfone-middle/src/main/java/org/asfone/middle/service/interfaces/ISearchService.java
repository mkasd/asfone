package org.asfone.middle.service.interfaces;

import org.asfone.core.dto.PublicationSearchRequest;
import org.asfone.core.model.Publication;
import org.asfone.middle.search.exception.SearchException;

import java.util.List;

/**
 * @author Oleksandr Dibrivnyi
 *         <p>
 *         2/18/2015
 */
public interface ISearchService {

    List<Publication> listAll() throws SearchException;

    Publication findOne(PublicationSearchRequest searchRequest) throws SearchException;

    List<Publication> findMultiple(PublicationSearchRequest searchRequest) throws SearchException;
}

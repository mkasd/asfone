package org.asfone.middle.service.interfaces;

import org.asfone.core.model.User;

public interface IRegistrationService {

    void registerUser(User user);
}

package org.asfone.middle.service.config;

import org.asfone.middle.dao.config.DaoConfiguration;
import org.asfone.middle.search.config.LuceneConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configures services layer with importing @link{DaoConfiguration} context
 *
 * @author oleksandr.dibrivnyi
 */
@Configuration
@Import({DaoConfiguration.class, LuceneConfiguration.class})
@ComponentScan(basePackages = {"org.asfone.middle.service.impl"})
public class ServiceConfiguration {

}

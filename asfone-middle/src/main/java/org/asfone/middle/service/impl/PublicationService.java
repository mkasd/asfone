package org.asfone.middle.service.impl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.core.dto.DocumentType;
import org.asfone.core.model.Publication;
import org.asfone.middle.dao.impl.PublicationFileDao;
import org.asfone.middle.dao.impl.PublicationRepository;
import org.asfone.middle.service.interfaces.IPublicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class PublicationService implements IPublicationService {

    private static final Logger log = LogManager.getLogger(PublicationService.class.getSimpleName());

    private PublicationFileDao publicationFileDao;

    private PublicationRepository publicationRepository;

    /**
     * Initializes @link{DocumentService} object and perform role of inject
     * point
     *
     * @param publicationFileDao {@link PublicationFileDao} injected object
     * @param publicationDao     {@link PublicationDao} injected object
     */
    @Autowired
    public PublicationService(PublicationFileDao publicationFileDao, PublicationRepository publicationRepository) {
        this.publicationFileDao = publicationFileDao;
        this.publicationRepository = publicationRepository;
    }

    @Override
    public void savePublication(Publication doc, File documentFile) {
        try {
            String documentExtension = FilenameUtils.getExtension(documentFile.getPath());
            DocumentType docType = getDocumentType(documentExtension);
            doc.setDocumentType(docType);

            byte[] data = FileUtils.readFileToByteArray(documentFile);

            publicationFileDao.saveDocumentData(data, doc);
            publicationRepository.save(doc);

            log.info("Document {0} was added as a managed object to db and local FS store", doc.getTitle());
        } catch (IOException e) {
            // TODO handle it
        }
    }

    @Override
    public void deletePublication(long id) {
        String storagePath = publicationRepository.getDocumentStoragePath(id);

        publicationFileDao.deleteDocumentData(storagePath);
        publicationRepository.delete(id);

        log.info("Document {0} was deleted from database and local fs storage", id);
    }

    @Override
    public Publication findPublication(long id) {
        return publicationRepository.findOne(id);
    }

    @Override
    public List<Publication> findAll() {
        return publicationRepository.findAll();
    }

    @Override
    public List<Publication> findAll(Iterable<Long> ids) {
        return publicationRepository.findAll(ids);
    }

    private DocumentType getDocumentType(String documentExtension) {
        if (DocumentType.PDF.getExtension().toLowerCase().endsWith(documentExtension)) {
            return DocumentType.PDF;
        } else if (DocumentType.DOC.getExtension().toLowerCase().endsWith(documentExtension)) {
            return DocumentType.DOC;
        } else if (DocumentType.DOCX.getExtension().toLowerCase().endsWith(documentExtension)) {
            return DocumentType.DOCX;
        }
        return null;
    }
}

package org.asfone.middle.dao.impl;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asfone.core.dto.ApplicationPaths;
import org.asfone.core.exception.FileProccesingException;
import org.asfone.core.model.Publication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class PublicationFileDao {

    private static final Logger log = LogManager.getLogger(PublicationFileDao.class.getSimpleName());

    @Autowired
    private ApplicationPaths applicationPaths;

    /**
     * Saves document data to specified path in local FS
     *
     * @param data - byte array that contains document data
     * @param doc  - @link{Document} object
     */
    public void saveDocumentData(byte[] data, Publication doc) {
        String fileName = generetaFileName(doc.getTitle());
        String fullPathWithExtension = applicationPaths.getFilesPath().concat(fileName)
                .concat(doc.getDocumentType().getExtension());
        File documentFile = new File(fullPathWithExtension);
        try {
            FileUtils.writeByteArrayToFile(documentFile, data);
            doc.setStorePath(fullPathWithExtension);
            log.info("Document data {0} was saved to local FS", doc.getTitle());
        } catch (IOException e) {
            throw new FileProccesingException("Could not save file", e);
        }
    }

    /**
     * Deletes document data file with specified path
     *
     * @param fullPathWithExtension - path of document that must be deleted
     */
    public void deleteDocumentData(String fullPathWithExtension) {
        File docData = new File(fullPathWithExtension);
        boolean isDeleted = docData.delete();
        if (isDeleted) {
            log.info("File {0} was deleted from files storage", fullPathWithExtension);
        } else {
            throw new FileProccesingException("Could not delete file");
        }
    }

    private String generetaFileName(String title) {
        return title.replaceAll("\\W", "") + System.currentTimeMillis();
    }
}

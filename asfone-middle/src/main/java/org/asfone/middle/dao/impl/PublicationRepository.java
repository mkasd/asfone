package org.asfone.middle.dao.impl;

import org.asfone.core.model.Publication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicationRepository extends JpaRepository<Publication, Long> {

    @Query("SELECT p.storePath FROM Publication p where p.id = :id")
    String getDocumentStoragePath(@Param("id") long id);
}

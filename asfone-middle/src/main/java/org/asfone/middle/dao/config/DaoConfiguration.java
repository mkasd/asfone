package org.asfone.middle.dao.config;

import org.asfone.core.dto.ApplicationPaths;
import org.asfone.core.dto.DataSourceProperties;
import org.asfone.middle.helper.config.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Configures dao layer. Contains definitions of beans, needs to perform CRUD
 * actions
 *
 * @author oleksandr.dibrivnyi
 */
@Configuration
@ComponentScan(basePackages = {"org.asfone.middle.dao.impl"})
@EnableJpaRepositories(basePackages = {"org.asfone.middle.dao.impl"})
@EnableTransactionManagement
@Import({PropertiesConfiguration.class})
public class DaoConfiguration {

    @Autowired
    private ApplicationPaths applicationPaths;

    @Autowired
    private DataSourceProperties dataSourceProperties;

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(true);
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("org.asfone.core.model");
        factory.setDataSource(dataSource());

        factory.afterPropertiesSet();
        return factory.getObject();
    }

    /**
     * Configures @link{SQLiteDataSource} to connect sqlite database with
     *
     * @return - configured @link{SQLiteDataSource} object
     * @link{SQLiteConfig object
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(dataSourceProperties.getUrl());
        dataSource.setDriverClassName(dataSourceProperties.getDriverClassName());
        dataSource.setPassword(dataSourceProperties.getPassword());
        dataSource.setUsername(dataSourceProperties.getUsername());
        return dataSource;
    }
}

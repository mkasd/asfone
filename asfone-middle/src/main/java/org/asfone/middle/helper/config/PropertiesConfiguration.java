package org.asfone.middle.helper.config;

import org.asfone.core.dto.ApplicationPaths;
import org.asfone.core.dto.DataSourceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource({"classpath:app.properties"})
public class PropertiesConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public ApplicationPaths applicationPaths() {
        String filesPath = env.getProperty("files.store.path").replace("{user.home}", System.getProperty("user.home"));
        return new ApplicationPaths(filesPath);
    }

    @Bean
    public DataSourceProperties dataSourceProperties() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setDriverClassName(env.getProperty("driver.class"));
        dataSourceProperties.setPassword(env.getProperty("jdbc.password"));
        dataSourceProperties.setUsername(env.getProperty("jdbc.user"));
        dataSourceProperties.setUrl(env.getProperty("jdbc.url"));
        return dataSourceProperties;
    }
}
